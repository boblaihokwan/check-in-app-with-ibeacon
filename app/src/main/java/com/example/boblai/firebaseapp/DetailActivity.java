package com.example.boblai.firebaseapp;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.service.RangedBeacon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailActivity extends AppCompatActivity implements BeaconConsumer {
    TextView title, holder, description, location;
    ImageView icon_901,icon_904,icon_905,icon_921,icon_922,icon_924,icon_925;
    RecyclerView recyclerView;
    Button participation, checkIn, leave;

    FirebaseFirestore db;

    final static String ROOMS = "Rooms";

    DocumentReference docRef;
    String eventLocation = "";
    String currentLocation = "";

    private BeaconManager beaconManager;
    public static final String IBEACON_FORMAT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
    protected static final String TAG = "AddEventActivity";
    private static final double RANGING_DISTANCE = 3; //in meter
    Region region;
    boolean isHolder;
    EventDetailRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        View view = LayoutInflater.from(this).inflate(R.layout.event_detail_header, recyclerView, false);


        icon_901 = view.findViewById(R.id.icon_901);
        icon_904 = view.findViewById(R.id.icon_904);
        icon_905 = view.findViewById(R.id.icon_905);
        icon_921 = view.findViewById(R.id.icon_921);
        icon_922 = view.findViewById(R.id.icon_922);
        icon_924 = view.findViewById(R.id.icon_924);
        icon_925 = view.findViewById(R.id.icon_925);

        title = view.findViewById(R.id.title);
        holder = view.findViewById(R.id.holder);
        location = view.findViewById(R.id.location);
        description = view.findViewById(R.id.description);

        String docId = getIntent().getStringExtra("eventId");
        db = FirebaseFirestore.getInstance();
        docRef = db.collection(ROOMS).document(docId);

        Map<String, Object> data = new HashMap<>();
        data.put("User", UserConstant.Username);
        data.put("Status", "Absent");
        data.put("Location", "Unknown");
        data.put("timestamp", FieldValue.serverTimestamp());

        isHolder = getIntent().getBooleanExtra("isHolder", false);

        if(!isHolder){
            docRef.collection("Participant")
                    .document(UserConstant.UserId)
                    .set(data)
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                        }
                    });

            beaconManager = BeaconManager.getInstanceForApplication(this);
            beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(IBEACON_FORMAT));
            beaconManager.setForegroundScanPeriod(1200);
            RangedBeacon.setSampleExpirationMilliseconds(2000);
            beaconManager.bind(this);


        }else{
            view.findViewById(R.id.legend_your_location).setVisibility(View.GONE);
            view.findViewById(R.id.legend_your_location_text).setVisibility(View.GONE);
        }


        docRef.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){
                    title.setText(documentSnapshot.getString("Name"));
                    holder.setText(documentSnapshot.getString("Username"));
                    description.setText(documentSnapshot.getString("description"));
                    location.setText(documentSnapshot.getString("Location"));
                    eventLocation = documentSnapshot.getString("Location");
                    eventLocation = documentSnapshot.getString("Location");
                    setLocationMarker();
                }else{
                    if(!isHolder){
                        AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
                        builder.setTitle("Event Ended");
                        builder.setMessage(title.getText().toString() + " has ended.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                finish();
                            }
                        });
                        builder.create().show();
                    }

                }
            }
        });

        adapter = new EventDetailRecyclerAdapter(this, view, docRef);
        recyclerView.setAdapter(adapter);

    }
    private void setAllBlack(){
        icon_901.setColorFilter(Color.BLACK);
        icon_904.setColorFilter(Color.BLACK);
        icon_905.setColorFilter(Color.BLACK);
        icon_921.setColorFilter(Color.BLACK);
        icon_922.setColorFilter(Color.BLACK);
        icon_924.setColorFilter(Color.BLACK);
        icon_925.setColorFilter(Color.BLACK);
    }

    private void setLocationMarker(){
        setAllBlack();
        if(eventLocation.equals("901")) icon_901.setColorFilter(Color.RED);
        if(eventLocation.equals("904")) icon_904.setColorFilter(Color.RED);
        if(eventLocation.equals("905")) icon_905.setColorFilter(Color.RED);
        if(eventLocation.equals("921")) icon_921.setColorFilter(Color.RED);
        if(eventLocation.equals("922")) icon_922.setColorFilter(Color.RED);
        if(eventLocation.equals("924")) icon_924.setColorFilter(Color.RED);
        if(eventLocation.equals("925")) icon_925.setColorFilter(Color.RED);

        if(currentLocation.equals("901")) icon_901.setColorFilter(Color.GREEN);
        if(currentLocation.equals("904")) icon_904.setColorFilter(Color.GREEN);
        if(currentLocation.equals("905")) icon_905.setColorFilter(Color.GREEN);
        if(currentLocation.equals("921")) icon_921.setColorFilter(Color.GREEN);
        if(currentLocation.equals("922")) icon_922.setColorFilter(Color.GREEN);
        if(currentLocation.equals("924")) icon_924.setColorFilter(Color.GREEN);
        if(currentLocation.equals("925")) icon_925.setColorFilter(Color.GREEN);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(final Collection<Beacon> beacons, final Region region) {
                Log.i(TAG, "Start ranging " + region.getUniqueId());
                if (beacons.size() > 1) {
                    Collections.sort((List<Beacon>) beacons, new Comparator<Beacon>() {
                        @Override
                        public int compare(Beacon beacon, Beacon t1) {
                            if(beacon.getDistance() == t1.getDistance()){
                                return 0;
                            }else if(t1.getDistance() > beacon.getDistance()){
                                return -1;
                            }else{
                                return 1;
                            }
                        }
                    });
                }

                final ArrayList<Beacon> beaconArray = (ArrayList<Beacon>) beacons;
                for(int i = 0; i<beaconArray.size(); i++){
                    Log.i(TAG, beaconArray.get(i).getBluetoothName() + ", " + beaconArray.get(i).getBluetoothAddress() + " , rssi: " + beaconArray.get(i).getRssi() +
                            " is about " + beaconArray.get(i).getDistance() * 100 + " cm away.");
                }

                if(beacons.size() > 0) {
                    for (int i = 0; i < beaconArray.size(); i++) {
                        if (beaconArray.get(0).getDistance() < RANGING_DISTANCE) {
                            String beaconLoc = BeaconConstant.beacons.get(beaconArray.get(i).getBluetoothName());
                            if (beaconLoc == null)
                                beaconLoc = BeaconConstant.beacons.get(beaconArray.get(i).getBluetoothAddress());

                            if (beaconLoc != null && !currentLocation.equals(beaconLoc)) {
                                currentLocation = beaconLoc;
                                HashMap<String, Object> data = new HashMap<>();
                                String status = beaconLoc.equals(eventLocation) ? "Present" : "Absent";
                                data.put("Status", status);
                                data.put("Location", beaconLoc);
                                data.put("timestamp", FieldValue.serverTimestamp());

                                docRef.collection("Participant")
                                        .document(UserConstant.UserId)
                                        .update(data)
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                e.printStackTrace();
                                            }
                                        });
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        setLocationMarker();
                                    }
                                });
                            }

                            break;
                        } else {
                            break;
                        }
                    }


                }
            }
        });
        try {
            region = new Region("newbeacon", null, null, null);
            beaconManager.startRangingBeaconsInRegion(region);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.del){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("delete Event");
            builder.setMessage("Do you want to delete " + title.getText().toString() + " event?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    db.collection(ROOMS).document(docRef.getId()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            finish();
                        }
                    });
                }
            });
            builder.setNegativeButton("NO", null);
            builder.create().show();

            return true;
        }else{

            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(!isHolder){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Leave Event");
            builder.setMessage("Do you want to leave " + title.getText().toString() + " event?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    docRef.collection("Participant").document(UserConstant.UserId)
                            .delete()
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    e.printStackTrace();
                                }
                            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            finish();
                        }
                    });
                }
            });
            builder.create().show();
        }else{
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(isHolder){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.del_menu, menu);
            return true;
        }else{
            return super.onCreateOptionsMenu(menu);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            if(region != null)
                beaconManager.stopRangingBeaconsInRegion(region);
        }catch(RemoteException e){
            e.printStackTrace();
        }
    }
}
