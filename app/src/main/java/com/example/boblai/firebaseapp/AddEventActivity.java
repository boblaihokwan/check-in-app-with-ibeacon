package com.example.boblai.firebaseapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.service.RangedBeacon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class AddEventActivity extends AppCompatActivity implements View.OnClickListener, BeaconConsumer {
    ImageView icon_901,icon_904,icon_905,icon_921,icon_922,icon_924,icon_925;
    AutoCompleteTextView eventName;
    EditText eventDescription;
    String location = "";
    final static String ROOMS = "Rooms";

    boolean isClicked = false;

    private BeaconManager beaconManager;
    public static final String IBEACON_FORMAT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
    protected static final String TAG = "AddEventActivity";
    private static final double RANGING_DISTANCE = 3; //in meter
    Region region;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        eventName = findViewById(R.id.name);
        eventDescription = findViewById(R.id.description);

        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(IBEACON_FORMAT));
        beaconManager.setForegroundScanPeriod(1000);
        RangedBeacon.setSampleExpirationMilliseconds(2000);
        beaconManager.bind(this);

        icon_901 = findViewById(R.id.icon_901);
        icon_904 = findViewById(R.id.icon_904);
        icon_905 = findViewById(R.id.icon_905);
        icon_921 = findViewById(R.id.icon_921);
        icon_922 = findViewById(R.id.icon_922);
        icon_924 = findViewById(R.id.icon_924);
        icon_925 = findViewById(R.id.icon_925);

        icon_901.setOnClickListener(this);
        icon_904.setOnClickListener(this);
        icon_905.setOnClickListener(this);
        icon_921.setOnClickListener(this);
        icon_922.setOnClickListener(this);
        icon_924.setOnClickListener(this);
        icon_925.setOnClickListener(this);

        setAllBlack();


    }

    private void setAllBlack(){
        icon_901.setColorFilter(Color.BLACK);
        icon_904.setColorFilter(Color.BLACK);
        icon_905.setColorFilter(Color.BLACK);
        icon_921.setColorFilter(Color.BLACK);
        icon_922.setColorFilter(Color.BLACK);
        icon_924.setColorFilter(Color.BLACK);
        icon_925.setColorFilter(Color.BLACK);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.add){
            item.setEnabled(false);

            String name = eventName.getText().toString();
            String description = eventDescription.getText().toString();

            // TODO: Error handling

            HashMap<String, Object> map = new HashMap<>();
            map.put("Name", name);
            map.put("description", description);
            map.put("UserId", UserConstant.UserId);
            map.put("Username", UserConstant.Username);
            map.put("Location", location);
            map.put("timestamp", FieldValue.serverTimestamp());

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection(ROOMS).add(map)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Intent intent = new Intent(AddEventActivity.this, DetailActivity.class);
                            intent.putExtra("eventId", documentReference.getId());
                            intent.putExtra("isHolder", true);
                            startActivity(intent);
                            finish();
                        }
                    });

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        setAllBlack();
        ((ImageView) view).setColorFilter(Color.RED);
        if(view == icon_901) location = "901";
        if(view == icon_904) location = "904";
        if(view == icon_905) location = "905";
        if(view == icon_921) location = "921";
        if(view == icon_922) location = "922";
        if(view == icon_924) location = "924";
        if(view == icon_925) location = "925";

        try{
            beaconManager.stopRangingBeaconsInRegion(region);
        }catch(RemoteException e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            beaconManager.stopRangingBeaconsInRegion(region);
        }catch(RemoteException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(final Collection<Beacon> beacons, final Region region) {
                Log.i(TAG, "Start ranging " + region.getUniqueId());
                if (beacons.size() > 1) {
                    Collections.sort((List<Beacon>) beacons, new Comparator<Beacon>() {
                        @Override
                        public int compare(Beacon beacon, Beacon t1) {
                            if(beacon.getDistance() == t1.getDistance()){
                                return 0;
                            }else if(t1.getDistance() > beacon.getDistance()){
                                return -1;
                            }else{
                                return 1;
                            }
                        }
                    });
                }

                final ArrayList<Beacon> beaconArray = (ArrayList<Beacon>) beacons;
                for(int i = 0; i<beaconArray.size(); i++){
                    Log.i(TAG, beaconArray.get(i).getBluetoothAddress() + " , rssi: " + beaconArray.get(i).getRssi() +
                            " is about " + beaconArray.get(i).getDistance() * 100 + " cm away.");
                }

                if(beacons.size() > 0){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for(int i=0;i<beaconArray.size();i++){
                                if(beaconArray.get(i).getDistance() < RANGING_DISTANCE){
                                    String beaconLoc = BeaconConstant.beacons.get(beaconArray.get(i).getBluetoothName());
                                    if(beaconLoc == null) beaconLoc = BeaconConstant.beacons.get(beaconArray.get(i).getBluetoothAddress());

                                    if(beaconLoc != null){
                                        location = beaconLoc;
                                        setAllBlack();
                                        if(location.equals("901")) icon_901.setColorFilter(Color.RED);
                                        if(location.equals("904")) icon_904.setColorFilter(Color.RED);
                                        if(location.equals("905")) icon_905.setColorFilter(Color.RED);
                                        if(location.equals("921")) icon_921.setColorFilter(Color.RED);
                                        if(location.equals("922")) icon_922.setColorFilter(Color.RED);
                                        if(location.equals("924")) icon_924.setColorFilter(Color.RED);
                                        if(location.equals("925")) icon_925.setColorFilter(Color.RED);
                                        break;
                                    }
                                }else{
                                    break;
                                }
                            }

                        }
                    });
                }
            }
        });
        try {
            region = new Region("newbeacon", null, null, null);
            beaconManager.startRangingBeaconsInRegion(region);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
