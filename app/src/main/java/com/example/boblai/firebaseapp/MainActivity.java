package com.example.boblai.firebaseapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button button;
    TextView userInfo;
    ListView listView;
    RecyclerView recyclerView;

    FirebaseFirestore db;

    final static String ROOMS = "Rooms";

    List<HashMap<String, String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button2);
        button.setText("Add Event");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addData();
            }
        });

        getSupportActionBar().setTitle("Welcome " + UserConstant.Username + "!");

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        EventListRecyclerAdapter eventListRecyclerAdapter = new EventListRecyclerAdapter(this);
        recyclerView.setAdapter(eventListRecyclerAdapter);
        db = FirebaseFirestore.getInstance();
    }

    private void addData(){
        Intent intent = new Intent(this, AddEventActivity.class);
        startActivity(intent);
    }
}
