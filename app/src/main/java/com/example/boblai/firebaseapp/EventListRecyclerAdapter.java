package com.example.boblai.firebaseapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Bob Lai on 3/4/2018.
 */

public class EventListRecyclerAdapter extends RecyclerView.Adapter {
    //ArrayList<HashMap<String, String>> data;
    private ArrayList<HashMap<String, String>> own;
    private ArrayList<HashMap<String, String>> other;

    final static String OWN_SECTION = "Own Event";
    final static String OTHER_SECTION = "Other Event";

    FirebaseFirestore db;
    final static String ROOMS = "Rooms";
    Context context;

    public EventListRecyclerAdapter(Context context){
        this.context = context;
        db = FirebaseFirestore.getInstance();
        db.collection(ROOMS)
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                //data = new ArrayList<>();

                own = new ArrayList<>();
                other = new ArrayList<>();

                for(QueryDocumentSnapshot doc: queryDocumentSnapshots){
                    final HashMap<String, String> map = new HashMap<>();
                    Date date = doc.getDate("timestamp");
                    String time = "";
                    if(date != null){
                        time = DateFormat.format("dd-MM-yyyy hh:mm", date).toString();
                    }
                    map.put("Title", doc.getString("Name") + "@" + doc.getString("Location"));
                    map.put("subtitle", "Hold by " + doc.getString("Username") + "     " + time);
                    map.put("id", doc.getId());
                    map.put("Holder", doc.getString("UserId"));
                    map.put("Name", doc.getString("Name"));
                    //data.add(map);
                    if(doc.getString("UserId").equals(UserConstant.UserId)){
                        own.add(map);
                    }else{
                        other.add(map);
                    }
                    notifyDataSetChanged();
                    doc.getReference().collection("Participant").addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                            map.put("count", "" + queryDocumentSnapshots.size());
                            notifyDataSetChanged();
                        }
                    });


                }

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        if((own == null || own.size() == 0) && (other == null || other.size() == 0)){
            return 0;
        }else{
            return 1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 1){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, parent, false);
            return new EventViewHolder(view);
        }else if(viewType == 0){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_event_list_item, parent, false);
            return new NoEventViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getClass().equals(NoEventViewHolder.class)) return;

        final HashMap<String, String> map;
        String sectiontitle = "";
        if(position <= own.size() - 1){
            if(position == 0){
                sectiontitle = OWN_SECTION;
            }
            map = own.get(position);
        }else{
            if(own == null){
                sectiontitle = OTHER_SECTION;
                map = other.get(position);
            }else{
                if (position == own.size()){
                    sectiontitle = OTHER_SECTION;
                }
                map = other.get(position - (own.size()));
            }


        }

        EventViewHolder eventViewHolder = (EventViewHolder) holder;
        eventViewHolder.title.setText(map.get("Title"));
        eventViewHolder.subTitle.setText(map.get("subtitle"));
        eventViewHolder.numOfParticipants.setText(map.get("count"));
        eventViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(map.get("Holder").equals(UserConstant.UserId)){
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("eventId", map.get("id"));
                    intent.putExtra("isHolder", map.get("Holder").equals(UserConstant.UserId));
                    context.startActivity(intent);
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Join Event");
                    builder.setMessage("Do you want to join " + map.get("Name") + " ?");
                    builder.setNegativeButton("No", null);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(context, DetailActivity.class);
                            intent.putExtra("eventId", map.get("id"));
                            intent.putExtra("isHolder", map.get("Holder").equals(UserConstant.UserId));
                            context.startActivity(intent);
                        }
                    });

                    builder.create().show();
                }

            }
        });
        if(!sectiontitle.equals("")){
            eventViewHolder.section.setVisibility(View.VISIBLE);
            eventViewHolder.section.setText(sectiontitle);
        }else{
            eventViewHolder.section.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(own != null) size += own.size();
        if(other != null) size += other.size();
        if(size == 0) return 1;
        else return size;
    }

    private static class EventViewHolder extends RecyclerView.ViewHolder{
        TextView numOfParticipants, title, subTitle, section;

        public EventViewHolder(View itemView) {
            super(itemView);
            numOfParticipants = itemView.findViewById(R.id.participants);
            title = itemView.findViewById(R.id.title);
            subTitle = itemView.findViewById(R.id.subtitle);
            section = itemView.findViewById(R.id.section);
        }
    }

    private static class NoEventViewHolder extends RecyclerView.ViewHolder{

        public NoEventViewHolder(View itemView) {
            super(itemView);
        }
    }
}
