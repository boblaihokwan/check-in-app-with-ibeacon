package com.example.boblai.firebaseapp;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Bob Lai on 3/4/2018.
 */

public class EventDetailRecyclerAdapter extends RecyclerView.Adapter {
    View headerView;

    final static int HEADER_VIEW = 0;
    final static int NORMAL_VIEW = 1;

    DocumentReference docRef;
    ArrayList<HashMap<String, String>> list;

    public EventDetailRecyclerAdapter(Activity activity, View headerView, DocumentReference docRef){
        this.docRef = docRef;
        setHeaderView(headerView);
        docRef.collection("Participant")
                .addSnapshotListener(activity, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                        list = new ArrayList<>();
                        for(QueryDocumentSnapshot doc: queryDocumentSnapshots){
                            HashMap<String, String> datum = new HashMap<>();
                            Log.d("datum", doc.toString());
                            datum.put("title", doc.getString("User") + " (" + doc.getString("Status") + ")");
                            Date date = doc.getDate("timestamp");
                            String time = "";
                            if(date != null){
                                time = DateFormat.format("dd-MM-yyyy hh:mm", date).toString();
                            }

                            String status = doc.getString("Status");
                            datum.put("Status", status);
                            datum.put("subtitle", "Currently at " + doc.getString("Location") + "  " + time);
                            list.add(datum);


                        }

                        notifyDataSetChanged();

                    }
                });
    }
    public void setHeaderView(View view){
        this.headerView = view;
    }

    public ArrayList<HashMap<String, String>> getList(){
        return list;
    };

    @Override
    public int getItemViewType(int position) {
        if(position == 0) return HEADER_VIEW;
        else return NORMAL_VIEW;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == HEADER_VIEW){
            return new HeaderViewHolder(headerView);
        }else if(viewType == NORMAL_VIEW){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.participant_list_item, parent, false);
            return new ParticipantViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getClass().equals(HeaderViewHolder.class)){
            // do nothing
        }else if(holder.getClass().equals(ParticipantViewHolder.class)){

            ParticipantViewHolder participantViewHolder = (ParticipantViewHolder) holder;
            if(list == null || list.size() == 0){
                participantViewHolder.title.setText("Sorry");
                participantViewHolder.subtitle.setText("No Participants");
                participantViewHolder.status.setImageResource(R.drawable.ic_sentiment_dissatisfied_black_24dp);
            }else{
                HashMap<String, String> datum = list.get(position - 1);
                participantViewHolder.title.setText(datum.get("title"));
                participantViewHolder.subtitle.setText(datum.get("subtitle"));
                if(datum.get("Status").equals("Present")){
                    participantViewHolder.status.setImageResource(R.drawable.ic_check_black_24dp);
                }else if(datum.get("Status").equals("Absent")){
                    participantViewHolder.status.setImageResource(R.drawable.ic_directions_run_black_24dp);
                }
            }



        }
    }

    @Override
    public int getItemCount() {
        if(list == null) return 2;
        if(list.size() == 0) return 2;
        return list.size() + 1;
    }

    private static class HeaderViewHolder extends RecyclerView.ViewHolder{
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    private static class ParticipantViewHolder extends RecyclerView.ViewHolder {
        TextView title, subtitle, section;
        ImageView status;
        public ParticipantViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            subtitle = itemView.findViewById(R.id.subtitle);
            status = itemView.findViewById(R.id.status);
        }
    }
}
