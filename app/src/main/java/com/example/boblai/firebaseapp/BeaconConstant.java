package com.example.boblai.firebaseapp;

import java.util.HashMap;

/**
 * Created by Bob Lai on 3/4/2018.
 */

public class BeaconConstant {
    public static HashMap<String, String> beacons;
    public static void initMap(){
        beacons = new HashMap<>();
        beacons.put("12:3B:6A:1B:09:CB", "905");
        beacons.put("12:3B:6A:1B:09:C3", "925");
        beacons.put("12:3B:6A:1B:09:D9", "924");
        beacons.put("12:3B:6A:1B:08:3E", "922");
        beacons.put("12:3B:6A:1B:08:07", "921");
        beacons.put("12:3B:6A:1B:09:BE", "901");
        beacons.put("12:3B:6A:1B:05:51", "904");
    }
}
