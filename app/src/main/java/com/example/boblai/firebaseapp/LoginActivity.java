package com.example.boblai.firebaseapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {


    private static final int REQUEST_READ_CONTACTS = 0;

    private AutoCompleteTextView mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mEmailSignInButton;
    Button mEmailRegisterButton;
    private TextView info;
    private boolean isLogin = true;
    FirebaseFirestore db;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        BeaconConstant.initMap();
        mAuth = FirebaseAuth.getInstance();

        SharedPreferences preferences = getSharedPreferences("Username", Context.MODE_PRIVATE);


        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);
        mUsernameView.setText(preferences.getString("Username", ""));
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setText(preferences.getString("Password", ""));
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin(true);
            }
        });

        mEmailRegisterButton = (Button) findViewById(R.id.email_register_button);
        mEmailRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin(false);
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        info = findViewById(R.id.info);

        db = FirebaseFirestore.getInstance();


        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1234);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //FirebaseUser user = firebaseAuth.getCurrentUser();
    }



    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin(final boolean isLogin) {
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String password = mPasswordView.getText().toString();
        final String username = mUsernameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if(TextUtils.isEmpty(username)){
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }


        if (cancel) {
            focusView.requestFocus();
            return;
        }

        CollectionReference collectionReference = db.collection("User");
        Query query = collectionReference.whereEqualTo("Username", username);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful() ){
                    if(task.getResult().size() > 0 && isLogin){
                        QueryDocumentSnapshot snapshots = task.getResult().iterator().next();
                        if(snapshots.getData().get("Password").equals(password)){
                            //login
                            UserConstant.Username = (String) snapshots.getData().get("Username");
                            UserConstant.UserId = (String) snapshots.getId();
                            SharedPreferences.Editor editor = getSharedPreferences("Username", Context.MODE_PRIVATE).edit();
                            editor.putString("Username", username);
                            editor.putString("Password", password);
                            editor.apply();
                            Log.d("Login", "Login suss");
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }else if(task.getResult().size() == 0 && !isLogin){
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("Username", username);
                        map.put("Password", password);

                        db.collection("User")
                                .add(map)
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        e.printStackTrace();
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        UserConstant.UserId = documentReference.getId();
                                        UserConstant.Username = username;
                                        SharedPreferences.Editor editor = getSharedPreferences("Username", Context.MODE_PRIVATE).edit();
                                        editor.putString("Username", username);
                                        editor.putString("Password", password);
                                        editor.apply();

                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                    }
                }
            }
        });

        if (!isLogin) {


            setEnableButton(false);
        } else {

            setEnableButton(false);
        }
    }

    private void setEnableButton(boolean enable){
        mEmailRegisterButton.setEnabled(enable);
        mEmailSignInButton.setEnabled(enable);
    }


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }



}





